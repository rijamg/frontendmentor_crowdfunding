let hamburger = document.querySelector('.hamburger');
let topheader = document.querySelector('.top-header');
let menu = document.querySelector('.menu');
let body = document.querySelector('body');
const fadeElems = document.querySelector('.has-fade');
let overlay = document.querySelector('.overlay');

hamburger.addEventListener('click', function(){
  if (topheader.classList.contains('open')){
    topheader.classList.remove('open');
    menu.classList.remove('selected');
    body.classList.remove('noscroll');
    fadeElems.classList.remove('fade-in');
    fadeElems.classList.add('fade-out');
  }
  else {
    topheader.classList.add('open');
    menu.classList.add('selected');
    body.classList.add('noscroll');
    fadeElems.classList.remove('fade-out');
    fadeElems.classList.add('fade-in');
  }
})


let bookSelect = document.querySelector('.book-select');
let bookmark = document.querySelector('.bookmark');

bookSelect.addEventListener('click', function(){
  if (bookSelect.classList.contains('filter-green')){
    bookSelect.classList.remove('filter-green');
    bookmark.innerHTML = 'Bookmark';
    bookmark.style.color = 'hsl(0, 0%, 48%)';
  }
  else {
    bookSelect.classList.add('filter-green');
    bookmark.innerHTML = 'Bookmarked';
    bookmark.style.color = 'hsl(176, 50%, 47%)';
  }
})

let backProject = document.querySelector('.back-project');
let modal = document.querySelector('.modal');
let selectReward = document.querySelector('.select-reward');
let selectReward2 = document.querySelector('.select-reward2');


function back(){
  if (modal.classList.contains('selected')){
    modal.classList.remove('selected');
    body.classList.remove('noscroll');
    fadeElems.classList.remove('fade-in');
    fadeElems.classList.add('fade-out');
    overlay.classList.remove('fade-general')
  }
  else {
    modal.classList.add('selected');

    body.classList.add('noscroll');
    fadeElems.classList.remove('fade-out');
    fadeElems.classList.add('fade-in');
    overlay.classList.add('fade-general')
      $(document).ready(function () {
        $("input[name='radio-button']").prop('checked', false);
    });
    }
}

backProject.addEventListener('click', back)
selectReward.addEventListener('click', back)
selectReward2.addEventListener('click', back)


document.querySelector('.modal-unselect').addEventListener('click', function(){
  modal.classList.remove('selected');
  body.classList.remove('noscroll');
  fadeElems.classList.remove('fade-in');
  fadeElems.classList.add('fade-out');
  overlay.classList.remove('fade-general')
})

let inputs=document.querySelectorAll("input[type=radio]");
let noRewards = document.querySelector('.noRewards');
let pledge25 = document.querySelector('.pledge25');
let pledge75 = document.querySelector('.pledge75');
let pledge200 = document.querySelector('.pledge200');

x=inputs.length;


while(x--)
inputs[x].addEventListener("change",function(){
      let radioSelect = document.querySelector('.'+ this.value);
      noRewards.classList.remove('radio-selected');
      pledge25.classList.remove('radio-selected');
      pledge75.classList.remove('radio-selected');
      pledge200.classList.remove('radio-selected');
      radioSelect.classList.add('radio-selected');
    },0);
